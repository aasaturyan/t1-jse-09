package ru.t1.aasaturyan.tm.api;

import ru.t1.aasaturyan.tm.model.Argument;

public interface IArgumentRepository {

    Argument[] getArguments();

}
