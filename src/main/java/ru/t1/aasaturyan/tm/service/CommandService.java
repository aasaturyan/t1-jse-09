package ru.t1.aasaturyan.tm.service;

import ru.t1.aasaturyan.tm.api.ICommandRepository;
import ru.t1.aasaturyan.tm.api.ICommandService;
import ru.t1.aasaturyan.tm.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

}
