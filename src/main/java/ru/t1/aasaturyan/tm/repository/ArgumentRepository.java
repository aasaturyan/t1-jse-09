package ru.t1.aasaturyan.tm.repository;

import ru.t1.aasaturyan.tm.api.IArgumentRepository;
import ru.t1.aasaturyan.tm.constant.ArgumentConst;
import ru.t1.aasaturyan.tm.model.Argument;

public final class ArgumentRepository implements IArgumentRepository {

    private static final Argument INFO = new Argument(
            ArgumentConst.INFO,
            "Show system info."
    );

    private static final Argument ABOUT = new Argument(
            ArgumentConst.ABOUT,
            "Show developer info."
    );

    private static final Argument HELP = new Argument(
            ArgumentConst.HELP,
            "Show command list"
    );

    private static final Argument VERSION = new Argument(
            ArgumentConst.VERSION,
            "Show application version."
    );

    public static final Argument[] COMMAND_ARGS = new Argument[]{
            ABOUT, INFO, VERSION, HELP
    };

    public Argument[] getArguments() {
        return COMMAND_ARGS;
    }
}
